#!/usr/bin/env node

var program = require('commander');
var fs = require('fs-extra');
var install = require('spawn-npm-install');

program
  .version('0.0.5')
  .option('-u, update [dep|dev|all]', 'Update packages. dep|dev|all', /^(dep|dev|all)$/i)
  .parse(process.argv);

  // .option('-c, check [dep|dev|all]', 'List packages', /^(dep|devdep)$/i)

// Current Working Directory
var cwdir = process.cwd();

// Automated Help
if (!process.argv.slice(2).length) {
  program.outputHelp();
}

// Update Dependencies
if (typeof program.update !== 'undefined') {
  // terminal argument
  var dependence = program.update;
  // package.json path
  var packageJSON = require(cwdir + '/package.json');
  if (packageJSON) {
    if (dependence === 'dep') {
      console.log('Updating saved project dependencies:');

      var regularDependencies = packageJSON.dependencies
      for(var dep in regularDependencies) {
        // console.log(regularDependencies[dep]);
        install(dep, { save: true, stdio: 'inherit' }, function(err) {
          if (err) { console.error(err.message); }
        });
      }
    } else if (dependence === 'dev') {
      console.log('Updating saved project development dependencies:');

      var devDependencies = packageJSON.devDependencies
      for(var dev in devDependencies) {
        install(dev, { saveDev: true, stdio: 'inherit' }, function(err) {
          if (err) { console.error(err.message); }
        });
      }
    } else if (dependence === 'all') {
      console.log('Updating all saved project dependencies and development dependencies:');

      var regularDependencies = packageJSON.dependencies
      var devDependencies = packageJSON.devDependencies
      for(var dep in regularDependencies) {
        for(var dev in devDependencies) {
          install(dev, { saveDev: true, stdio: 'inherit' }, function(err) {
            if (err) { console.error(err.message); }
          });
        }
        install(dep, { save: true, stdio: 'inherit' }, function(err) {
          if (err) { console.error(err.message); }
        });
      }
    }
  } else {
    console.log('Your package.json cannot be found.');
  }
}
