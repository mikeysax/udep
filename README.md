# udep

Simply update your node projects saved `package.json` dependencies in one command. This does not check which packages are out of date and update them; it simply just updates all of the packages you select.

Problem: Other update scripts do not update everything or see all outdated dependencies.

Solution: `udep` is not biased and will update every package under dependencies or devDependencies.

___
### Installation

<strong>To Install *udep*:</strong>

```$ npm install udep -g```

To see a list of commands:

```$ udep```
___

### Usage

Use all these commands inside your project directory in your terminal.

<strong>To Update regular dependencies:</strong>

```$ udep update dep```

or

```$ udep -u dep```

<strong>To Update development dependencies:</strong>

```$ udep update dev```

or

```$ udep -u dev```

<strong>To Update all dependencies:</strong>

```$ udep update all```

or

```$ udep -u all```
___
